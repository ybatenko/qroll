# frozen_string_literal: true

require "rails/generators/base"

module Qroll
  module Generators
    # Installer
    class InstallGenerator < Rails::Generators::Base
      source_root File.expand_path("../templates", __dir__)

      desc "Creates a Qroll initializer for your application."

      def copy_initializer
        template "qroll.rb", "config/initializers/qroll.rb"
      end
    end
  end
end
