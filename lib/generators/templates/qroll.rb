# frozen_string_literal: true

Qroll.configure do |config|
  config.json_backend = :active_support

  # config.logger = Rails.logger

  # config.rabbitmq_url = "amqp://guest:guest@127.0.0.1:5672"

  # config.rabbitmq_fanout = ENV["AMQP_FANOUT"]

  # config.rabbitmq_queue = ENV["AMQP_QUEUE"]

  # config.rabbitmq_options = { logger: config.logger, heartbit_timeout: 8 }
end

Qroll.subscribe!
