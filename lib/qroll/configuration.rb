# frozen_string_literal: true

require "logger"

module Qroll
  # Base configuration class
  class Configuration # # rubocop:disable Metrics/ClassLength
    DEFAULT_RABBITMQ_URL = "amqp://guest:guest@127.0.0.1:5672"

    attr_accessor :rabbitmq_url,
                  :rabbitmq_fanout,
                  :rabbitmq_queue,
                  :rabbitmq_options,
                  :logger

    attr_reader   :json_backend

    def initialize
      @json_backend = default_json_backend
      @logger       = default_logger

      @rabbitmq_url     = ENV.fetch("AMQP_URL", ENV.fetch("RABBITMQ_URL", DEFAULT_RABBITMQ_URL))
      @rabbitmq_fanout  = ENV.fetch("AMQP_FANOUT", ENV.fetch("RABBITMQ_FANOUT", "ex.#{application_name}"))
      @rabbitmq_queue   = ENV.fetch("AMQP_QUEUE", ENV.fetch("RABBITMQ_QUEUE", "qe.#{application_name}"))
      @rabbitmq_options = default_rabbitmq_options
    end

    def publisher_callback_class # rubocop:disable Metrics/MethodLength
      case @publisher_callback_class
      when :active_record
        require "qroll/resource/publisher/active_record_callbacks"
        Qroll::Resource::Publisher::ActiveRecordCallbacks
      when :postgresql_listener
        require "qroll/resource/publisher/postgresql_listener"
        Qroll::Resource::Publisher::PostgresqlListener
      else
        raise Qroll::ListenerBackendError,
              "Publisher callback class not defined! It should be either: :active_record or :postgresql_listener"
      end
    end

    def publisher_callback_class=(klass)
      case klass
      when :active_record, :postgresql_listener
        @publisher_callback_class = klass
      else
        raise Qroll::ListenerBackendError,
              "Publisher callback class not defined! It should be either: :active_record or :postgresql_listener"
      end
    end

    def json_backend=(backend) # rubocop:disable Metrics/MethodLength
      @json_backend =
        case backend
        when :oj, :oj_strict
          try_oj
        when :oj_rails
          try_oj(mode: :rails)
        when :active_support
          try_active_support
        when nil, :default, :json
          default_json_backend
        else
          raise Qroll::UnsupportedBackend,
                "Unsupported backend: #{backend}. Use one of: :oj, :oj_strict, :oj_rails, \
:active_support, :json (:default)"
        end
    end

    private

    JSON_BACKEND = Struct.new(:encode, :decode)
    private_constant :JSON_BACKEND

    def try_oj(mode: :strict)
      require "oj"
      JSON_BACKEND.new(->(hash) { Oj.dump(hash, mode: mode) }, ->(json) { Oj.load(json) })
    rescue LoadError
      Kernel.warn "`Oj` is not installed, falling back to default JSON encoder."
      default_json_backend
    end

    def try_active_support
      require "active_support/json"
      JSON_BACKEND.new(->(hash) { ActiveSupport::JSON.encode(hash) }, ->(json) { ActiveSupport::JSON.decode(json) })
    rescue LoadError
      Kernel.warn "`ActiveSupport` is not installed, falling back to default JSON backend."
      default_json_backend
    end

    def default_json_backend
      require "json"
      JSON_BACKEND.new(->(hash) { JSON.dump(hash) }, ->(json) { JSON.parse(json) })
    end

    def default_logger
      logfile = File.join(root_path, "log/rabbitmq_#{environment}.log")

      ActiveSupport::Logger.new(logfile).tap do |logger|
        logger.level = log_level
      end
    end

    def default_rabbitmq_options
      {
        logger: logger,
        log_level: log_level,
        heartbeat_timeout: 8,
        connection_name: "#{application_name}.app"
      }
    end

    def application_name
      @application_name ||=
        if defined?(Rails)
          Rails.application.class.module_parent_name.underscore
        elsif ENV["APP_NAME"].present?
          ENV["APP_NAME"]
        else
          "_no_name_"
        end
    end

    def environment
      ENV["RACK_ENV"].presence ||
        ENV["RAILS_ENV"].presence ||
        Rails.env
    end

    def root_path
      Rails.root.to_s
    end

    def log_level
      level = ENV["LOGGER_LEVEL"]
      case level
      when String
        "Logger::#{level}".constantize
      else
        Logger::WARN
      end
    end
  end
end
