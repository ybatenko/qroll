# frozen_string_literal: true

require "bunny"
require "singleton"
require "active_support"

module Qroll
  # RabbitMQ Bunny wrapper class
  class Rabbit
    include Singleton

    mattr_accessor :message_handler, default: {}, instance_accessor: false
    attr_reader :consumer

    def publish(payload, route)
      fanout.publish(
        encode_json(payload),
        durable: true,
        routing_key: route
      )
    end

    def subscribe # rubocop:disable Metrics/AbcSize, Metrics/MethodLength
      options = { block: false, manual_ack: true }

      @consumer = queue.subscribe(**options) do |delivery_info, _metadata, payload|
        logger.info "Receive payload: #{payload.inspect}"

        data = decode_json(payload)
        action = data.delete(:action)

        subscriber = self.class.message_handler[action]
        if subscriber
          subscriber.on_receive(data)
        else
          logger.error "No Subscriber on route: #{action}"
        end

        channel.acknowledge(delivery_info.delivery_tag, false)
      end
    end

    def unsubscribe
      consumer&.cancel
      channel.close
    end

    def connection
      @connection ||=
        begin
          options = configuration.rabbitmq_options
          Bunny.new(configuration.rabbitmq_url, options).tap(&:start)
        end
    end

    def channel
      @channel ||= connection.create_channel
    end

    def fanout
      @fanout ||= channel.fanout(configuration.rabbitmq_fanout, durable: true)
    end

    def queue
      @queue ||= channel.queue(configuration.rabbitmq_queue, durable: true)
    end

    def encode_json(payload)
      Qroll.json.encode.call(payload)
    end

    def decode_json(payload)
      Qroll.json.decode.call(payload).symbolize_keys
    end

    def logger
      Qroll.logger
    end

    def configuration
      Qroll.configuration
    end
  end
end
