# frozen_string_literal: true

module Qroll
  module Resource
    # Base class for Publisher
    class BasicPublisher
      RESERVED_ATTR_NAMES = %i[action timestamp model_name changes].freeze
      attr_reader :_attributes, :model_class

      def initialize(target)
        @model_class = target
        @_attributes = {}
      end

      # rubocop:disable Metrics/MethodLength, Metrics/CyclomaticComplexity
      def _materialize(obj)
        action = if obj.destroyed?
                   :delete
                 elsif obj.previous_changes[:created_at]
                   :create
                 else
                   :update
                 end

        { action: action }.tap do |h|
          _attributes.each do |key, value|
            next if key == :changes && %i[create delete].include?(action)

            h[key] = case value
                     when Proc
                       obj.instance_exec(&value)
                     when BasicPublisher, Publisher
                       value._materialize(obj)
                     else
                       value
                     end
          end
          h[:model_name] = obj.model_name.singular
        end
      end
      # rubocop:enable Metrics/MethodLength, Metrics/CyclomaticComplexity

      # Metaprogramming macroses
      # options reserved to conditions
      def attribute(name, **options, &block)
        raise ArgumentError, "No block given in `attribute` definition" unless block
        raise ArgumentError, "Already defined attribute `#{name}`!" if @_attributes[name.to_sym]
        raise ArgumentError, "Reserved attribute name: #{name}" if RESERVED_ATTR_NAMES.include?(name.to_sym)

        @_attributes[name.to_sym] = [block, options]
      end

      # rubocop:disable Metrics/AbcSize, Metrics/MethodLength, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity
      def attributes(*attrs, except: nil, only: nil)
        raise ArgumentError, "Block not expected in `attributes` definition" if block_given?

        existing_fields = model_class.column_names.map(&:to_sym)
        attrs = attrs.any? ? attrs.flatten.compact.uniq.map(&:to_sym) : existing_fields

        diff = RESERVED_ATTR_NAMES & attrs
        raise ArgumentError, "Reserved attribute name(s): #{diff.inspect}" if diff.present?

        if attrs.present? && (except || only)
          raise ArgumentError, "Can't pass attributes with `except:` or `only` options"
        end

        if except
          attrs = existing_fields - except
        elsif only
          attrs = only
        end

        raise ArgumentError, "Attribute list cannot be empty!" if attrs.empty?

        model_fields_and_methods = model_class.instance_methods + existing_fields

        attrs.each do |attr_name|
          raise ArgumentError, "Already defined attribute `#{attr_name}`!" if @_attributes[attr_name]

          unless model_fields_and_methods.include?(attr_name)
            raise ArgumentError, "Unknown attribute `#{attr_name}` in model `#{model_class.name}`"
          end

          @_attributes[attr_name] = proc { send(attr_name) }
        end
      end
      # rubocop:enable Metrics/AbcSize, Metrics/MethodLength, Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity

      def namespace(name, &block)
        raise ArgumentError, "No block given in `namespace` definition" unless block
        raise ArgumentError, "Already defined namespace `#{name}`!" if @_attributes[name.to_sym]
        raise ArgumentError, "`namespace` expects name" if name&.to_s&.strip.blank?

        @_attributes[name.to_sym] =
          BasicPublisher.new(model_class).tap { |pub| pub.instance_eval(&block) }
      end
    end

    # DSL for build resoulting payload to be published
    # when action fired
    class Publisher < BasicPublisher
      def initialize(target)
        super
        @route = @prefix = nil
      end

      def _build_payload
        raise ArgumentError, "Route key not set!" unless @route

        { prefix: @prefix, route: @route }.merge(@_attributes)
      end

      def prefix(arg)
        @prefix = arg.to_s.strip

        raise ArgumentError, "Prefix should not be empty" if @prefix.blank?
      end

      def route(arg)
        @route = arg.to_s.strip

        raise ArgumentError, "Route should not be empty" if @route.blank?
      end

      # rubocop:disable Naming/MethodParameterName
      def uuid(arg = proc { SecureRandom.uuid }, as: :uuid, &block)
        raise ArgumentError, "Already defined attribute `#{as}`!" if @_attributes[as.to_sym]

        if block_given?
          @_attributes[as.to_sym] = block
        else
          raise ArgumentError, "Uuid argument should be callable!" unless arg.respond_to?(:call)

          @_attributes[as.to_sym] = arg
        end
      end

      def timestamp(arg = proc { Time.current.iso8601 }, as = :timestamp, &block)
        raise ArgumentError, "Already defined attribute `#{as}`!" if @_attributes[as.to_sym]

        if block_given?
          @_attributes[as.to_sym] = block
        else
          raise ArgumentError, "Timestamp argument should be callable!" unless arg.respond_to?(:call)

          @_attributes[as.to_sym] = arg
        end
      end

      def changes(arg = proc { _normalize_changes }, as = :changes, &block)
        raise ArgumentError, "Already defined attribute `#{as}`!" if @_attributes[as.to_sym]

        if block_given?
          @_attributes[as.to_sym] = block
        else
          raise ArgumentError, "Changes argument should be callable!" unless arg.respond_to?(:call)

          @_attributes[as.to_sym] = arg
        end
      end
      # rubocop:enable Naming/MethodParameterName
    end
  end
end
