# frozen_string_literal: true

module Qroll
  module Resource
    class Publisher
      # Base
      class BaseHandler
        POSSIBLE_CALLBACKS = %i[all create destroy update].freeze
        attr_reader :action, :model_class

        def initialize(action, model_class)
          @action      = action
          @model_class = model_class

          raise ArgumentError, "Unknown callback action `#{@action}`" unless POSSIBLE_CALLBACKS.include?(@action)
        end
      end
    end
  end
end
