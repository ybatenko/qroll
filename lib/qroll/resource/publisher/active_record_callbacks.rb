# frozen_string_literal: true

require_relative "base_handler"
require "active_record"

module Qroll
  module Resource
    class Publisher
      # ActiveRecord callbacks installer
      class ActiveRecordCallbacks < BaseHandler
        # rubocop:disable  Metrics/AbcSize, Metrics/MethodLength
        def setup_callbacks(materializer)
          # rubocop:disable Metrics/BlockLength
          model_class.class_exec(action, model_class, materializer) do |action_, model_, materializer_|
            define_callbacks :_publish_to_qroll

            with_options if: -> { @_skip_qroll_callback } do
              if action_ == :all
                model_.set_callback :create,  :after, :_publish_to_qroll
                model_.set_callback :destroy, :after, :_publish_to_qroll
                model_.set_callback :update,  :after, :_publish_to_qroll
              else
                model_.set_callback action, :after, :_publish_to_qroll
              end
            end

            define_method :_publish_to_qroll do
              payload = materializer_._materialize(self)
              Qroll.publish(payload)
            end

            private :_publish_to_qroll

            def _normalize_changes(fields_to_skip = %w[updated_at
                                                       created_at
                                                       password
                                                       encrypted_password token
                                                       reset_password_token
                                                       reset_password_sent_at
                                                       remember_created_at
                                                       confirmation_token
                                                       confirmation_sent_at])
              normalized_changes = {}
              previous_changes.except(*fields_to_skip).each do |field, (old_value, new_value)|
                normalized_changes[field.to_sym] = { old: old_value, new: new_value } # corresponds with PG triggers
              end
              normalized_changes
            end

            private :_normalize_changes
          end
          # rubocop:enable Metrics/BlockLength
        end
        # rubocop:enable Metrics/AbcSize, Metrics/MethodLength
      end
    end
  end
end
