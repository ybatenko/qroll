# frozen_string_literal: true

require "active_record"

module Qroll
  module Resource
    # Patch to ActiveRecord to skip Qroll callbacks on create, update, delete
    module QrollCallbackSupressor
      refine ActiveRecord::Base do
        def save(*)
          @_skip_qroll_callback = true
          super
        end

        def save!(*)
          @_skip_qroll_callback = true
          super
        end
      end
    end

    # Receive payload from Rabbit channel
    class Subscriber
      attr_reader :model_class, :payload

      def initialize(route, target, block)
        @route           = route
        @model_class     = target
        @subscribe_block = block
      end

      def assign_changes(object)
        changes = payload[:changes]
        return if changes.blank?

        attrs = changes.transform_values { |v| v[:new] }
        object.assign_attributes(attrs)
      end

      def on_receive(payload)
        @payload = payload
        @subscribe_block.binding.eval("using QrollCallbackSupressor")
        instance_exec(&@subscribe_block)
      end
    end
  end
end
