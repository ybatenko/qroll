# frozen_string_literal: true

require "qroll/resource/publisher"
require "qroll/resource/subscriber"

module Qroll
  # Module attaches to correspondant model's resource
  module Resource
    def self.included(base)
      super
      base.extend ClassMethods
    end

    # Metaprogramming macroses
    #
    module ClassMethods
      # Describe what to publish on model change
      #
      def publish(action: :all, &block)
        model = model_class
        Resource::Publisher.new(model).tap do |publisher|
          publisher.instance_eval(block)
          Qroll.add_model_changes_watcher(action, model, publisher._build_payload)
        end
      end

      # Describe RabbitMQ queue and topic
      #
      def subscribe(route:, &block)
        route = route.to_s
        raise ArgumentError, "Invalid subscriber's route name `#{route}`" if route.empty?
        raise ArgumentError, "Subscriber's route `#{route}` already exist!" if message_handler[route]

        message_handler[route] = Resource::Subscriber.new(route, model_class, block)
      end

      def message_handler
        Qroll::Rabbit.message_handler
      end

      # Infere model's class from resource
      #
      def model_class
        model_name_str = name.sub(/(Resource|Qroll)$/, "")
        Object.const_get(model_name_str)
      rescue NameError
        raise NameError, "Qroll resource expects to be named as ModelResource or ModelQroll, but its name is #{name}"
      end

      private :model_class
    end
  end
end
