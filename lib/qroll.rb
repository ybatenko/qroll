# frozen_string_literal: true

require_relative "qroll/version"
require_relative "qroll/rabbit"
require_relative "qroll/configuration"
require_relative "qroll/resource"

# Base module
module Qroll
  class Error < StandardError; end
  UnsupportedBackend = Class.new(Error)
  ListenerBackendError = Class.new(Error)

  class << self
    attr_accessor :configuration

    def configure
      self.configuration ||= Configuration.new
      yield configuration if block_given?

      configuration
    end

    # Publishing payload into RabbitMQ queue with routing key route
    #
    def publish(payload, route)
      rabbit.publush(payload, route)
    end

    # Run subscription to RabbitMQ exchange
    #
    def subscribe!
      rabbit.subscribe
    end

    def json
      configuration.json_backend
    end

    def logger
      configuration.logger
    end

    def rabbit
      Rabbit.instance
    end

    # Setup AR callback
    #
    def add_model_changes_watcher(action, model, payload)
      configuration
        .publisher_callback_class
        .new(action, model)
        .setup_callbacks(payload)
    end
  end
end
