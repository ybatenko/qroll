# frozen_string_literal: true

require "test_helper"

# Models are in test/dummy/app/models/...

describe Qroll::Resource do
  class CompanyResource
    include Qroll::Resource
  end

  describe "extend base" do
    it "Resource extended ClassMethods" do
      expect(CompanyResource.constants).must_include :Publisher
      expect(CompanyResource.constants).must_include :Subscriber
    end
  end

  describe "inferring AR model" do
    it "infere underlying model" do
      expect(CompanyResource.send :model_class).must_equal Company
    end

    module Product
      class LineItemQroll
        include Qroll::Resource
      end
    end

    it "infere namespaced model" do
      expect(Product::LineItemQroll.send :model_class).must_equal Product::LineItem
    end

    class UnknownResource
      include Qroll::Resource
    end

    it "raises exception if model not known" do
      expect { UnknownResource.send :model_class }.must_raise NameError
    end
  end
end
