# frozen_string_literal: true

require "test_helper"
require "qroll/resource/publisher/active_record_callbacks"

# Models are in test/dummy/app/models/...

describe Qroll::Resource::Publisher do
  let(:klass) { Qroll::Resource::Publisher }
  let(:callbacks_klass) { Qroll::Resource::Publisher::ActiveRecordCallbacks }

  describe "validations" do

    it "allows only known actions" do
      expect { callbacks_klass.new(:unknown, Class.new(User)) }.must_raise ArgumentError
    end
  end

  describe "model's callbacks" do
    it "attach callbacks :all" do

      User3Class = Class.new(User)
      obj = klass.new(User3Class)

      obj.instance_eval do
        prefix "web-shop"
        route "user"
        uuid
        timestamp
        changes
        attributes :name, :phone, :status, :nickname
      end
      obj
        .tap do |r|
        callbacks_klass
          .new(:all, User3Class)
          .setup_callbacks(r)
      end

      user = User3Class.new(name: "Name", phone: "+7", status: 1)
      Qroll.expects(:publish).with(has_entry(action: :create)).returns(nil)
      assert user.save

      user.name = "Vasya"
      Qroll.expects(:publish).with(has_entry(action: :update)).returns(nil)
      assert user.save

      Qroll.expects(:publish).with(has_entry(action: :delete)).returns(nil)
      assert user.destroy
    end
  end
end
