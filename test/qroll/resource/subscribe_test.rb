# frozen_string_literal: true

require "test_helper"

# Models are in test/dummy/app/models/...

describe Qroll::Resource::Subscriber do
  describe "parsing and saving payload" do
    class UserResource
      include Qroll::Resource

      subscribe route: "user.update" do
        model = model_class.find(payload[:id])

        assign_changes(model)

        model.save!
      end
    end

    it "evaluate `subscribe` block in context of resource" do
      user = User.create(name: "Matz", phone: "+12 111111")
      payload = { id: user.id,
                  name: "Ko1",
                  phone: "+7 101",
                  changes: {
                    name: { old: "Matz", new: "Ko1" },
                    phone: { old: "+12 111111", new: "+7 101" }
                  }
                }

      UserResource.message_handler["user.update"].on_receive(payload)

      user.reload

      expect(user.name).must_equal payload[:changes][:name][:new]
      expect(user.phone).must_equal payload[:changes][:phone][:new]
    end
  end

  describe "skipping Qroll callbacks on save record" do
    class Product::LineItemResource
      include Qroll::Resource

      subscribe route: "product/line_item.update" do
        model = model_class.find(payload[:id])

        assign_changes(model)

        model.save!
      end
    end

  end
end
