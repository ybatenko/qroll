# frozen_string_literal: true

require "test_helper"

# Models are in test/dummy/app/models/...

describe Qroll::Resource::Publisher do
  let(:klass) { Qroll::Resource::Publisher }

  describe "validations" do
    before do
      @obj = klass.new(Class.new(User))
    end

    it "#prefix expects not empty string" do
      expect { @obj.prefix("") }.must_raise ArgumentError
    end

    it "#route expects not empty string" do
      expect { @obj.route("") }.must_raise ArgumentError
    end

    # attribute

    it "#attribute demands block" do
      expect { @obj.attribute(:attr) }.must_raise ArgumentError
    end

    it "#attribute cannot be defined more then once" do
      @obj.attribute(:attr) do
      end

      expect {
        @obj.attribute(:attr) do
        end
      }.must_raise ArgumentError
    end

    # attributes

    it "#attributes don't accept block" do
      expect {
        @obj.attributes(:attr) do
        end
      }.must_raise ArgumentError
    end

    it "#attributes accepts only model's accessors" do
      expect { @obj.attributes(:unknown) }.must_raise ArgumentError
    end

    # uuid

    it "#uuid should be defined only once" do
      @obj.uuid
      expect { @obj.uuid }.must_raise ArgumentError
    end

    it "#uuid argument should be callable" do
      expect { @obj.uuid(:not_lambda) }.must_raise ArgumentError
    end

    # timestamp

    it "#timestamp should be defined only once" do
      @obj.timestamp
      expect { @obj.timestamp }.must_raise ArgumentError
    end

    it "#timestamp argument should be callable" do
      expect { @obj.timestamp(:not_lambda) }.must_raise ArgumentError
    end

    # changes

    it "#changes should be defined only once" do
      @obj.changes
      expect { @obj.changes }.must_raise ArgumentError
    end

    it "#changes argument should be callable" do
      expect { @obj.changes(:not_lambda) }.must_raise ArgumentError
    end

    # namespace

    it "#namespace demands block" do
      expect { @obj.namespace }.must_raise ArgumentError
    end
  end # validations


  describe "building list of attributes" do

    it "collects all given attributes" do
      UserClass = Class.new(User)

      obj = klass.new(UserClass)

      obj.instance_eval do

        prefix "web-shop"
        route "user"
        uuid
        timestamp

        attribute "updated-at" do
          updated_at.utc
        end

        attribute "totals" do
          42
        end

        attributes :name, :phone, :status, :nickname

        # debug helpers
        def get(name)
          @_attributes[name.to_sym]
        end

        def get_var(name)
          instance_variable_get :"@#{name}"
        end
      end # obj

      expect(obj.get_var(:prefix)).must_equal "web-shop"
      expect(obj.get_var(:route)).must_equal "user"
      expect(obj.get(:uuid)).must_be_kind_of Proc
      expect(obj.get(:uuid).call).must_be_kind_of String
      expect(obj.get(:timestamp)).must_be_kind_of Proc
      expect(obj.get(:timestamp).call).must_be_kind_of String
      expect(obj.get("updated-at").first).must_be_kind_of Proc
      expect(obj.get("totals").first).must_be_kind_of Proc
      expect(obj.get(:name)).must_be_kind_of Proc
      expect(obj.get(:phone)).must_be_kind_of Proc
      expect(obj.get(:status)).must_be_kind_of Proc
      expect(obj.get(:nickname)).must_be_kind_of Proc
    end

    # TODO: add tests for attributes except:, only:
  end

  describe "building with namespaces" do

    it "uses namespaces" do
      User2Class = Class.new(User)
      obj = klass.new(User2Class)

      obj.instance_eval do
        prefix "web-shop"
        route "user"
        namespace "attributes" do
          attributes :name, :phone, :status, :nickname
        end

        # debug helpers
        def get(name)
          @_attributes[name.to_sym]
        end

        def get_var(name)
          instance_variable_get :"@#{name}"
        end
      end # obj

      expect(obj.get(:attributes)).must_be_kind_of Qroll::Resource::BasicPublisher
    end
  end
end
