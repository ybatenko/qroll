# frozen_string_literal: true

require "test_helper"

describe Qroll::Configuration do
  describe "default values" do
    before { Qroll.configuration = nil }

    subject { Qroll.configuration }

    it "provides default values" do
      Qroll.configure

      save_amqp_env, ENV["AMQP_URL"] = ENV["AMQP_URL"], nil

      expect(subject.rabbitmq_url).must_equal Qroll::Configuration::DEFAULT_RABBITMQ_URL
      expect(subject.rabbitmq_fanout).must_equal "ex.dummy"
      expect(subject.rabbitmq_queue).must_equal "qe.dummy"
      expect(subject.json_backend).must_respond_to :encode
      expect(subject.json_backend).must_respond_to :decode
      expect(subject.logger).must_be_kind_of ActiveSupport::Logger

      ENV["AMQP_URL"] = save_amqp_env
    end
  end

  describe "configure settings" do
    let(:url)         { "amqp://root:root@192.168.0.0:11111" }
    let(:node_fanout) { "web_node" }
    let(:node_queue)  { "web_queue" }

    subject { Qroll.configuration }

    it "allow change values" do
      Qroll.configure do |config|
        config.rabbitmq_url = url
        config.rabbitmq_fanout = node_fanout
        config.rabbitmq_queue = node_queue
      end

      expect(subject.rabbitmq_url).must_equal url
      expect(subject.rabbitmq_fanout).must_equal node_fanout
      expect(subject.rabbitmq_queue).must_equal node_queue
    end

    it "raises error when json_backend unknown" do
      expect {
        Qroll.configure do |config|
          config.json_backend = :unknown
        end
      }.must_raise Qroll::UnsupportedBackend
    end

    it "raises error when publisher_callback_class unknown" do
      expect {
        Qroll.configure do |config|
          config.publisher_callback_class = :unknown
        end
      }.must_raise Qroll::ListenerBackendError
    end
  end
end
