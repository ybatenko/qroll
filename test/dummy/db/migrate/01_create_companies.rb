# frozen_string_literal: true

class CreateCompanies < (Rails::VERSION::STRING >= '5' ? ActiveRecord::Migration[5.0] : ActiveRecord::Migration)
  def change
    create_table :companies do |t|
      t.string :title, null: false
      t.string :description
      t.string :address

      t.timestamps null: false
    end
  end
end
