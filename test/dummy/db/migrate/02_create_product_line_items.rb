# frozen_string_literal: true

class CreateProductLineItems < (Rails::VERSION::STRING >= '5' ? ActiveRecord::Migration[5.0] : ActiveRecord::Migration)
  def change
    create_table :product_line_items do |t|
      t.string :name, null: false
      t.string :description
      t.integer :price_cents

      t.timestamps null: false
    end
  end
end
