# frozen_string_literal: true

class Product::LineItem < ActiveRecord::Base
  belongs_to :company
end
