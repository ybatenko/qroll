# frozen_string_literal: true

class Company < ActiveRecord::Base
  has_many :users
  has_many :goods, class_name: "Product::LineItem"
end
