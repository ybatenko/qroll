# frozen_string_literal: true

ENV["RACK_ENV"] = "test"

require "warning"
Gem.path.each do |path|
  Warning.ignore(//, path)
end

require "pry"
require "minitest/autorun"
require "minitest/focus"
require "mocha/minitest"

require File.expand_path("../../test/dummy/config/environment.rb", __FILE__)

ActiveRecord::Migration.verbose = false
verbosity, ENV['VERBOSE'] = ENV['VERBOSE'], 'false'
if ActiveRecord::Migrator.respond_to? :migrate
  ActiveRecord::Migrator.migrate(ActiveRecord::Migrator.migrations_paths = [File.expand_path("../../test/dummy/db/migrate", __FILE__)])
else
  ActiveRecord::Migrator.migrations_paths << File.expand_path('../dummy/db/migrate', __FILE__)
  ActiveRecord::Tasks::DatabaseTasks.drop_current 'test'
  ActiveRecord::Tasks::DatabaseTasks.create_current 'test'
  ActiveRecord::Tasks::DatabaseTasks.migrate
end
ENV['VERBOSE'] = verbosity

$LOAD_PATH.unshift File.expand_path("../lib", __dir__)
require "qroll"
