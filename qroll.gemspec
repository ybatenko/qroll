# frozen_string_literal: true

require_relative "lib/qroll/version"

Gem::Specification.new do |spec|
  spec.name          = "qroll"
  spec.version       = Qroll::VERSION
  spec.authors       = ["Yury Batenko"]
  spec.email         = ["jurbat@gmail.com"]

  spec.summary       = "Qroll is the wrapper for Bunny to befriend it with Rails models."
  spec.description   = <<-DESC
    Qroll adds ability to unobtrusively map ActiveRecord model attributes to RabbitMQ messages back and forth.
  DESC
  spec.homepage      = "https://github.com/svenyurgensson/qroll"
  spec.license       = "MIT"
  spec.required_ruby_version = Gem::Requirement.new(">= 2.7.0")

  spec.metadata["homepage_uri"] = spec.homepage
  spec.metadata["source_code_uri"] = "https://github.com/svenyurgensson/qroll"
  spec.metadata["changelog_uri"] = "https://github.com/svenyurgensson/qroll/blob/master/CHANGELOG.md"

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files = Dir.chdir(File.expand_path(__dir__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{\A(?:test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{\Aexe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_dependency "bunny", ">= 2.17"
  spec.add_dependency "rails", ">= 6.0"

  spec.add_development_dependency "bundler"
  spec.add_development_dependency "minitest-focus"
  spec.add_development_dependency "minitest-rails"
  spec.add_development_dependency "mocha"
  spec.add_development_dependency "oj"
  spec.add_development_dependency "pry-byebug"
  spec.add_development_dependency "rake"
  spec.add_development_dependency "sqlite3"
  spec.add_development_dependency "warning"
end
